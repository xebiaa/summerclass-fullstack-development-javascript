var fs = require('fs');

var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var router = express.Router();

app.use('/api', bodyParser.json());
app.use('/api', router);
app.listen(3000);

var employees = [
  {id: 1, name: 'John'},
  {id: 2, name: 'Peter'}
];

router.get('/employees', function(req, res) {
  res.json(employees);
});

var clients = [
  {id: 1, name: 'Company A'},
  {id: 2, name: 'Company B'}
];

router.get('/clients', function(req, res) {
  res.json(clients);
});

function HashTable() {
  this.weeks = {};
}

HashTable.prototype = {
  get: function (key) {
    return this.weeks[key];
  },
  put: function (key, value) {
    this.weeks[key] = value;
  }
};

function Timesheet() {
  this.weeks = {};
}

Timesheet.prototype = {
  getAll: function (user) {
    return this.weeks[user];
  },
  getWeek: function (user, week) {
    if(this.weeks[user] !== undefined) {
      return this.weeks[user].get(week);
    }
  },
  put: function (user, week, value) {
    this.weeks[user] = new HashTable();
    this.weeks[user].put(week, value);
  }
};

var timesheets = new Timesheet();
timesheets.put(1, 30, {
  companies: [
    {id: 1, hours: {mon: 1, tue: 1, wed: 1, thu: 1, fri:1}},
    {id: 2, hours: {mon: 1, tue: 1, wed: 3, thu: 1, fri:1}}
  ]
});

router.route('/timesheet/:user')
  .get(function (req, res) {
    var userTimesheets = timesheets.getAll(req.params.user);
    if(userTimesheets === undefined) {
      res.send(404);
      return;
    }

    res.json(userTimesheets);
  });

router.route('/timesheet/:user/:week')
  .get(function (req, res) {
    var timesheet = timesheets.getWeek(req.params.user, req.params.week);
    if(timesheet === undefined) {
      res.send(404);
      return;
    }

    res.json(timesheet);
  })
  .post(function (req, res) {
    timesheets.put(req.params.user, req.params.week, req.body);

    res.send(200);
  });
