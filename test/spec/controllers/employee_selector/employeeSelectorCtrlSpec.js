'use strict';

describe('Controller: employeeSelectorCtrl', function () {
  var $scope, $httpBackend, $cookieStore, employees, initialiseController;

  employees = [
    {id: 1, name: 'John'},
    {id: 2, name: 'Peter'}
  ];

  beforeEach(function () {
    module('timesheet');

    inject(function ($rootScope, $controller, _$httpBackend_, _$cookieStore_) {
      $scope = $rootScope.$new();
      $httpBackend = _$httpBackend_;
      $cookieStore = _$cookieStore_;

      initialiseController = function(setCookie, cookieValue) {
        if(setCookie) {
          $cookieStore.put('employee', cookieValue);
        }

        $controller('EmployeeSelectorCtrl', {
          $scope: $scope,
          $cookieStore: $cookieStore
        });

        $httpBackend.whenGET('/api/employees').respond(200, employees);
        $httpBackend.flush();
      }
    });
  });

  it('should retrieve 2 employees from the server', function () {
    initialiseController(false);

    expect($scope.employees.length).toBe(2);
  });

  describe('selected employee in scope', function () {
    it('should be set to the first result', function () {
      initialiseController(false);

      expect($scope.selectedEmployee.id).toEqual(employees[0].id);
    });

    it('should be set to the cookie value when the cookie is set', function () {
      initialiseController(true, employees[1].id);

      expect($scope.selectedEmployee.id).toEqual(employees[1].id);
    });
  });

  describe('cookie value', function () {
    it('should set the cookie value to the first result when the cookie is not set', function () {
      initialiseController(false);

      expect($cookieStore.get('employee')).toEqual(employees[0].id);
    });

    it('should set the cookie value to the selected employee when changing the dropdown', function () {
      initialiseController(false);
      $scope.selectedEmployee = employees[1];

      $scope.selectEmployee();

      expect($cookieStore.get('employee')).toEqual(employees[1].id);
    });
  });
});