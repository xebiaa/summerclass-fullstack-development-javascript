'use strict';

angular
  .module('timesheet', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ui.router'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/week/');

    $stateProvider
      .state('weekEntry', {
        url: '/week/:number',
        templateUrl: '/scripts/week_entry/weekEntry.html'
      });
  });