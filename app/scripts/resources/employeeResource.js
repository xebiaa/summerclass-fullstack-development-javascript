'use strict';

angular.module('timesheet')
  .factory('Employee', function ($resource) {
    return $resource(
      '/api/employees',
      {}
    );
  });