'use strict';

angular.module('timesheet')
  .factory('Client', function ($resource) {
    return $resource(
      '/api/clients',
      {}
    );
  });