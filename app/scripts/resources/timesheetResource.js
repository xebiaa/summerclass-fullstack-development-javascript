'use strict';

angular.module('timesheet')
  .factory('Timesheet', function ($resource) {
    return $resource(
      '/api/timesheet/:user/:week',
      {user: '@user', week: '@week'}
    );
  });