'use strict';

angular.module('timesheet')
  .controller('EmployeeSelectorCtrl', function ($scope, $cookieStore, Employee) {
    $scope.selectEmployee = function () {
      $cookieStore.put('employee', $scope.selectedEmployee.id);
    };

    Employee.query().$promise.then(function (employees) {
      $scope.employees = employees;
      $scope.selectedEmployee = $cookieStore.get('employee') === undefined ? employees[0] : findEmployee($cookieStore.get('employee'));

      if ($cookieStore.get('employee') === undefined) {
        $scope.selectEmployee();
      }
    });

    function findEmployee(employeeId) {
      return _.find($scope.employees, function (employee) {
        if (employee.id === employeeId) {
          return employee;
        }
      });
    }
  });