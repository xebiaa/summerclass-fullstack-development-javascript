/** @jsx React.DOM */
var WeekEntryComponent = React.createClass({
  getInitialState: function () {
    if (this.props.rows.length === 0) {
      this.props.rows[0] = {company: this.props.clients[0].id, hours: {mon: 0, tue: 0, wed: 0, thu: 0, fri: 0} };
    }

    return {rows: this.props.rows};
  },

  componentWillReceiveProps: function (nextProps) {
    this.replaceState({rows: nextProps.rows});
  },

  render: function () {
    var createRow = function (row, index) {
      return <WeekEntryRowComponent index={index}
      row={row}
      clients={this.props.clients}
      onClientChange={this.onClientChange}
      onTimeChange={this.onTimeChange}
      removeRow={this.removeRow} />;
    }.bind(this);

    return <div>
      <button className="btn btn-info" onClick={this.previousWeek}>
        <span className="glyphicon glyphicon-chevron-left"></span>
      Previous week
      </button>
      <button className="btn btn-info pull-right" onClick={this.nextWeek}>
        <span className="glyphicon glyphicon-chevron-right"></span>
      Next week
      </button>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Company</th>
            <th>Monday</th>
            <th>Tuesday</th>
            <th>Wednesday</th>
            <th>Thursday</th>
            <th>Friday</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {this.state.rows.map(createRow)}
        </tbody>
      </table>
      <div className="form-group">
        <button className="btn btn-info" onClick={this.addRow}>Add Client</button>
      &nbsp;
        <button className="btn btn-info" onClick={this.submitTimesheet}>Submit hours</button>
      </div>
    </div>;
  },

  previousWeek: function () {
    this.props.previousWeek();
  },

  nextWeek: function () {
    this.props.nextWeek();
  },

  addRow: function () {
    var currentRows = this.state.rows;
    currentRows[currentRows.length] = {company: this.props.clients[0].id, hours: {mon: 0, tue: 0, wed: 0, thu: 0, fri: 0}};
    this.replaceState({rows: currentRows});
  },

  removeRow: function (index) {
    if (this.state.rows[index] !== undefined) {
      this.state.rows.splice(index, 1);
      this.replaceState({rows: this.state.rows});
    }
  },

  submitTimesheet: function () {
    this.props.saveMethod(this.state.rows);
  },

  onClientChange: function (index, company) {
    var currentState = this.state.rows;

    currentState[index].company = company;
    for (var day in currentState[index].hours) {
      currentState[index].hours[day] = 0;
    }

    this.replaceState({rows: currentState});
  },

  onTimeChange: function (index, hours) {
    var currentState = this.state.rows;

    currentState[index].hours = hours;

    this.replaceState({rows: currentState});
  }
});

var WeekEntryRowComponent = React.createClass({
  onTimeChange: function () {
    this.props.onTimeChange(this.props.index, {
      mon: this.refs.mon.getDOMNode().value,
      tue: this.refs.tue.getDOMNode().value,
      wed: this.refs.wed.getDOMNode().value,
      thu: this.refs.thu.getDOMNode().value,
      fri: this.refs.fri.getDOMNode().value
    });
  },

  removeRow: function () {
    this.props.removeRow(this.props.index);
  },

  render: function () {
    var company = this.props.row;

    return <tr>
      <td>
        <WeekEntryClientDropDownComponent index={this.props.index} clients={this.props.clients} companyId={company.id} onClientChange={this.props.onClientChange}/>
      </td>
      <td>
        <input value={company.hours.mon} onChange={this.onTimeChange} size="2" className="form-control" ref="mon" />
      </td>
      <td>
        <input value={company.hours.tue} onChange={this.onTimeChange} size="2" className="form-control" ref="tue" />
      </td>
      <td>
        <input value= {company.hours.wed} onChange={this.onTimeChange} size="2" className="form-control" ref="wed" />
      </td>
      <td>
        <input value={company.hours.thu} onChange={this.onTimeChange} size="2" className="form-control" ref="thu" />
      </td>
      <td>
        <input value={company.hours.fri} onChange={this.onTimeChange} size="2" className="form-control" ref="fri" />
      </td>
      <td>
        {this.props.index !== 0 &&
          <button className="btn btn-info" onClick={this.removeRow}>
            <span className="glyphicon glyphicon-remove"></span>
          Remove
          </button>
          }
      </td>
    </tr>
  }
});

var WeekEntryClientDropDownComponent = React.createClass({
  onClientChange: function () {
    this.props.onClientChange(this.props.index, +this.refs.client.getDOMNode().value);
  },

  render: function () {
    return <select value={this.props.companyId} onChange={this.onClientChange} className="form-control" ref="client">
                {this.props.clients.map(function (client) {
                  return <option value={client.id}>{client.name}</option>
                })}
    </select>;
  }
});