'use strict';

angular.module('timesheet')
  .directive('weekEntry', function () {
    return {
      restrict: 'A',
      link: function (scope, element) {
        scope.$watchCollection('[clients, model]', function (newData) {
          if (newData[0] !== undefined && newData[1] !== undefined) {
            React.renderComponent(new WeekEntryComponent({
                rows: newData[1].companies,
                clients: newData[0],
                saveMethod: scope.saveTimesheet,
                previousWeek: scope.previousWeek,
                nextWeek: scope.nextWeek

              }
            ), element.find('#weekEntry')[0]);
          }
        });
      }
    };
  });