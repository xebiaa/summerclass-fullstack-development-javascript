'use strict';

angular.module('timesheet')
  .controller('WeekEntryCtrl', function ($scope, $cookies, $stateParams, $location, Client, Timesheet) {
    function getClients() {
      if ($scope.clients === undefined) {
        Client.query().$promise.then(function (clients) {
          $scope.clients = clients;
        });
      }
    }

    function getTimesheet(week) {
      $scope.currentWeek = week;

      Timesheet.get({user: $scope.user, week: week}).$promise.then(
        function (timesheet) {
          getClients();
          $scope.model = timesheet;
        },
        function () {
          getClients();
          $scope.model = {companies: []};
        }
      );
    }

    $scope.$watch(
      function () {
        return $cookies.employee;
      },
      function (newEmployeeId) {
        $scope.user = newEmployeeId;
        console.log('week', $scope.determineWeek());
        getTimesheet($scope.determineWeek());
      });

    $scope.saveTimesheet = function (rows) {
      var data = {companies: []};
      _(rows).forEach(function (row) {
        data.companies.push(row);
      });

      Timesheet.save({user: $scope.user, week: $scope.currentWeek}, data);
    };

    $scope.previousWeek = function () {
      $scope.changeWeek($scope.determineWeek() - 1);
    };

    $scope.nextWeek = function () {
      $scope.changeWeek($scope.determineWeek() + 1);
    };

    $scope.changeWeek = function (number) {
      $location.path('/week/' + number);
      $scope.$apply(); // http://ericsaupe.com/angularjs-location-not-changing-the-path/
    };

    $scope.determineWeek = function () {
      if($stateParams.number !== '') {
        return +$stateParams.number;
      }
      return new Date().getWeek();
    };

  });